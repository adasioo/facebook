Required software
-------------
* JDK 1.8+
* Apache Maven

Running unit tests
-------------

    mvn test

Running integration tests
-------------

    mvn verify

> Please note that before you run integration tests, make sure you've generated Facebook Access Token
> and placed the token in test's resource directory application.yml file

Running stand-alone application
-------------

    mvn spring-boot:run

> You can access service manually with your favourite http client, but make sure you have addedd
> authorization header eg.:

    Authorization: Bearer <facebook_access_token>

Build jar package
-------------

    mvn clean package
    
Generate a Basic User Access Token with Facebook Graph API explorer:
-------------

    https://developers.facebook.com/tools/explorer/
    
