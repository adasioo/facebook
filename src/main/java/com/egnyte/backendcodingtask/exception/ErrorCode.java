package com.egnyte.backendcodingtask.exception;

public enum ErrorCode {

  TOKEN_VALIDATION_ERROR("Invalid token", 1),
  FACEBOOK_CLIENT_ERROR("Facebook client fail.", 2);

  ErrorCode(String description, int code) {
    this.code = code;
    this.description = description;
  }

  private int code;
  private String description;
}
