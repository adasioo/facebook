package com.egnyte.backendcodingtask.exception;

import com.egnyte.backendcodingtask.dto.ErrorDTO;
import com.restfb.exception.FacebookOAuthException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class FacebookOauthExMapper implements ExceptionMapper<FacebookOAuthException> {

  @Override
  public Response toResponse(FacebookOAuthException e) {
    return Response.status(Response.Status.UNAUTHORIZED)
        .entity(new ErrorDTO(ErrorCode.TOKEN_VALIDATION_ERROR, "Please provide valid Facebook token."))
        .build();
  }
}
