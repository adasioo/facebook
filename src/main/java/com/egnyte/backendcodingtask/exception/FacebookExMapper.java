package com.egnyte.backendcodingtask.exception;

import com.egnyte.backendcodingtask.dto.ErrorDTO;
import com.restfb.exception.FacebookException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class FacebookExMapper implements ExceptionMapper<FacebookException> {

  @Override
  public Response toResponse(FacebookException e) {
    return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
        .entity(new ErrorDTO(ErrorCode.FACEBOOK_CLIENT_ERROR,
            "Ooops, something went wrong with facebook API."))
        .build();
  }
}
