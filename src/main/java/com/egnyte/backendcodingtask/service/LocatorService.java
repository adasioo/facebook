package com.egnyte.backendcodingtask.service;

import com.egnyte.backendcodingtask.dto.LocationDTO;
import com.egnyte.backendcodingtask.dto.mapper.LocationMapper;
import com.restfb.*;
import com.restfb.types.Place;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class LocatorService {

  private LocationMapper mapper;
  private FacebookClient facebookClient;

  @Autowired
  public LocatorService(LocationMapper mapper, FacebookClient facebookClient) {
    this.mapper = mapper;
    this.facebookClient = facebookClient;
  }

  public List<LocationDTO> findPlace(String query) {
    Connection<Place> locations = facebookClient.fetchConnection("search", Place.class,
        Parameter.with("q", query),
        Parameter.with("type", "place"),
        Parameter.with("fields", "name, location"));
    List<Place> places = locations.getData();
    return places.stream()
          .map(mapper::toDTO)
          .collect(toList());
  }
}
