package com.egnyte.backendcodingtask.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;

public class TokenFilter extends OncePerRequestFilter {

  public static final String BEARER = "Bearer ";

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws ServletException, IOException {

    String bearer = extractBearer(request);
    if (StringUtils.hasText(bearer) && verifyBearer(bearer)) {
      SecurityContextHolder.getContext().setAuthentication(
          new UsernamePasswordAuthenticationToken("USER", bearer, new ArrayList<>()));
    }

    filterChain.doFilter(request, response);
  }

  private boolean verifyBearer(String bearer) {
    // the token can be verified here
    return true;
  }

  private String extractBearer(HttpServletRequest request) {
    String requestHeader = request.getHeader(AUTHORIZATION);
    if (StringUtils.hasText(requestHeader) && requestHeader.toLowerCase().startsWith(BEARER.toLowerCase())) {
      return requestHeader.substring(BEARER.length(), requestHeader.length());
    }
    return null;
  }
}
