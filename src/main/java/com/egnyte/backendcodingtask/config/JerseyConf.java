package com.egnyte.backendcodingtask.config;

import com.egnyte.backendcodingtask.exception.FacebookExMapper;
import com.egnyte.backendcodingtask.exception.FacebookOauthExMapper;
import com.egnyte.backendcodingtask.web.LocationController;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class JerseyConf extends ResourceConfig {

  public JerseyConf() {
    registerMappers();
    registerEndpoints();
  }

  private void registerMappers() {
    register(FacebookOauthExMapper.class);
    register(FacebookExMapper.class);
  }

  private void registerEndpoints() {
    register(LocationController.class);
  }
}
