package com.egnyte.backendcodingtask.config;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Version;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

@Configuration
public class FacebookConf {

  @Context
  SecurityContext context;

  @Bean
  @Scope(value = "request", proxyMode= ScopedProxyMode.TARGET_CLASS)
  FacebookClient facebookClient() {
    String accessToken = (String) SecurityContextHolder.getContext().getAuthentication().getCredentials();
    return new DefaultFacebookClient(accessToken, Version.VERSION_2_9);
  }
}
