package com.egnyte.backendcodingtask.dto.mapper;

import com.egnyte.backendcodingtask.dto.LocationDTO;
import com.restfb.types.Place;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel="spring")
public interface LocationMapper {

  @Mappings({
                 @Mapping(source = "place.location.longitude", target = "longitude"),
                 @Mapping(source = "place.location.latitude", target = "latitude")
             } )
  LocationDTO toDTO(Place place);
}
