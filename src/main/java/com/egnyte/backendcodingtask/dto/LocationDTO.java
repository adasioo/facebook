package com.egnyte.backendcodingtask.dto;

import lombok.Data;

@Data
public class LocationDTO {

  private String name;
  private Double latitude;
  private Double longitude;

}
