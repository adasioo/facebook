package com.egnyte.backendcodingtask.dto;

import com.egnyte.backendcodingtask.exception.ErrorCode;
import lombok.Data;

@Data
public class ErrorDTO {

  private ErrorCode code;
  private String message;

  public ErrorDTO() {
  }

  public ErrorDTO(ErrorCode code, String message) {
    this.code = code;
    this.message = message;
  }
}
