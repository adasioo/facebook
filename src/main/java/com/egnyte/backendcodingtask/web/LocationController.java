package com.egnyte.backendcodingtask.web;

import com.egnyte.backendcodingtask.dto.LocationDTO;
import com.egnyte.backendcodingtask.service.LocatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/")
@Component
@Produces(MediaType.APPLICATION_JSON)
public class LocationController {

  private LocatorService locatorService;

  @Autowired
  public LocationController(LocatorService locatorService) {
    this.locatorService = locatorService;
  }

  @GET
  @Path("/{country}/{city}/{name}")
  public List<LocationDTO> getLocation(@PathParam("country") String country,
      @PathParam("city") String city,
      @PathParam("name") String name) {
    return locatorService.findPlace(country + "," + city + "," + name);
  }

}
