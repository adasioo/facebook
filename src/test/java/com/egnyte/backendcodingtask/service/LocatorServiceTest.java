package com.egnyte.backendcodingtask.service;

import com.egnyte.backendcodingtask.dto.LocationDTO;
import com.egnyte.backendcodingtask.dto.mapper.LocationMapper;
import com.egnyte.backendcodingtask.dto.mapper.LocationMapperImpl;
import com.restfb.Connection;
import com.restfb.FacebookClient;
import com.restfb.types.Location;
import com.restfb.types.Place;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class LocatorServiceTest {

  private LocatorService locatorService;

  @Mock private FacebookClient facebookClient;
  private LocationMapper mapper;

  public static final String EGNYTE_POZNAN_POLAND = "Egnyte Poland";
  public static final Double EGNYTE_POZNAN_POLAND_LATITUDE = 52.404167557908;
  public static final Double EGNYTE_POZNAN_POLAND_LONGITUDE = 16.940044275923;
  private static final String EGNYTE_QUERY = "Egnyte";

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);
    mapper = new LocationMapperImpl();
    locatorService = spy(new LocatorService(mapper, facebookClient));
  }

  @Test
  public void whenFbClientFetchSucessfullyConnectionThenReturnLocationDTOs() throws Exception {

    //given
    Connection places = mock(Connection.class);
    Place place = mock(Place.class);
    Location location = mock(Location.class);
    when(location.getLatitude()).thenReturn(EGNYTE_POZNAN_POLAND_LATITUDE);
    when(location.getLongitude()).thenReturn(EGNYTE_POZNAN_POLAND_LONGITUDE);
    when(place.getLocation()).thenReturn(location);
    when(place.getName()).thenReturn(EGNYTE_POZNAN_POLAND);
    List<Place> locations =  Arrays.asList(place);
    when(places.getData()).thenReturn(locations);
    when(facebookClient.fetchConnection(any(), any(), any(), any(), any())).thenReturn(places);

    //when
    List<LocationDTO> locationDTOList = locatorService.findPlace(EGNYTE_QUERY);

    //then
    assertNotNull(locationDTOList);
    assertEquals(1, locationDTOList.size());
    assertEquals(EGNYTE_POZNAN_POLAND, locationDTOList.get(0).getName());
    assertEquals(EGNYTE_POZNAN_POLAND_LATITUDE, locationDTOList.get(0).getLatitude(), 0);
    assertEquals(EGNYTE_POZNAN_POLAND_LONGITUDE, locationDTOList.get(0).getLongitude(), 0);
  }

}