package com.egnyte.backendcodingtask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendCodingTaskApplicationTests {

  public static void main(String[] args) {
    SpringApplication.run(BackendCodingTaskApplicationTests.class, args);
  }

}
