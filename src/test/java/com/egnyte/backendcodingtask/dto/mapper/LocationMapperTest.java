package com.egnyte.backendcodingtask.dto.mapper;

import com.egnyte.backendcodingtask.dto.LocationDTO;
import com.restfb.types.Location;
import com.restfb.types.Place;
import org.junit.Before;
import org.junit.Test;

import static com.egnyte.backendcodingtask.service.LocatorServiceTest.EGNYTE_POZNAN_POLAND;
import static com.egnyte.backendcodingtask.service.LocatorServiceTest.EGNYTE_POZNAN_POLAND_LATITUDE;
import static com.egnyte.backendcodingtask.service.LocatorServiceTest.EGNYTE_POZNAN_POLAND_LONGITUDE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LocationMapperTest {

  private  LocationMapper locationMapper;

  @Before
  public void init() {
    locationMapper = new LocationMapperImpl();
  }

  @Test
  public void shouldMapPlaceToLocationDTO() throws Exception {

    //given
    Place place = mock(Place.class);
    Location location = mock(Location.class);
    when(location.getLatitude()).thenReturn(EGNYTE_POZNAN_POLAND_LATITUDE);
    when(location.getLongitude()).thenReturn(EGNYTE_POZNAN_POLAND_LONGITUDE);
    when(place.getName()).thenReturn(EGNYTE_POZNAN_POLAND);
    when(place.getLocation()).thenReturn(location);

    //when
    LocationDTO locationDTO = locationMapper.toDTO(place);

    //then
    assertThat(locationDTO).isNotNull();
    assertThat(locationDTO.getLatitude()).isEqualTo(EGNYTE_POZNAN_POLAND_LATITUDE);
    assertThat(locationDTO.getLongitude()).isEqualTo(EGNYTE_POZNAN_POLAND_LONGITUDE);
    assertThat(locationDTO.getName()).isEqualTo(EGNYTE_POZNAN_POLAND);
  }
}