package com.egnyte.backendcodingtask.security;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.egnyte.backendcodingtask.security.TokenFilter.BEARER;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TokenFilterTest {

  private static final String TOKEN = "EAACEdEose0cBAL0WDu1omZAfpcO9lzPDhVHNPNo5ccFuZBiz7Ari9qGZAvpUWrrWREHf7bQEpkzabyCZBJEDHsYFtue3GBZALCT6ZBPg77pRrqvKVC0vT19ExiYtXGV16oikJwZAcc2szpzWFpE2Rv1f9WUJGbTneTYT0klmICCOv0ScNza4sppoT5JmWnm5EQZD";
  public static final String SAMPLE_EXPIRED_AUTH_BEARER_HEADER = BEARER + TOKEN;
  private TokenFilter filter;

  @Before
  public void setUp() throws Exception {
    filter = new TokenFilter();
  }

  @Test
  public void whenWellFormattedTokenThenSetContextAuthentication() throws Exception {

    //given
    HttpServletRequest request = mock(HttpServletRequest.class);
    HttpServletResponse response = mock(HttpServletResponse.class);
    FilterChain filterChain = mock(FilterChain.class);
    when(request.getHeader(AUTHORIZATION)).thenReturn(SAMPLE_EXPIRED_AUTH_BEARER_HEADER);

    //when
    filter.doFilterInternal(request, response, filterChain);

    //then
    assertNotNull(SecurityContextHolder.getContext().getAuthentication());
    assertEquals(TOKEN, SecurityContextHolder.getContext().getAuthentication().getCredentials());
    assertEquals("USER", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
  }
}