package com.egnyte.backendcodingtask.web;

import com.egnyte.backendcodingtask.BackendCodingTaskApplicationTests;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static com.egnyte.backendcodingtask.exception.ErrorCode.TOKEN_VALIDATION_ERROR;
import static com.egnyte.backendcodingtask.security.TokenFilter.BEARER;
import static com.egnyte.backendcodingtask.security.TokenFilterTest.SAMPLE_EXPIRED_AUTH_BEARER_HEADER;
import static com.egnyte.backendcodingtask.service.LocatorServiceTest.*;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.http.ContentType.JSON;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackendCodingTaskApplicationTests.class, webEnvironment= RANDOM_PORT)
public class LocationControllerTestIT {

  private static String LOCALHOST = "http://localhost:";
  private static String GET_LOCATION_URL;
  private static String GET_MULTIPLE_LOCATION_URL;

  @Value("${facebook.access.token}")
  private String token;

  @LocalServerPort
  private int port;

  @Before
  public void setUp() throws Exception {
    GET_LOCATION_URL = LOCALHOST + port + "/poland/poznan/egnyte";
    GET_MULTIPLE_LOCATION_URL = LOCALHOST + port + "/poland/opoczno/gmina";
  }

  @Test
  public void whenFacebook() throws Exception {
    given()
      .header(AUTHORIZATION, SAMPLE_EXPIRED_AUTH_BEARER_HEADER)
    .when()
      .get(GET_LOCATION_URL)
    .then()
      .assertThat()
        .statusCode(UNAUTHORIZED.getStatusCode())
      .and()
        .body("code", equalTo(TOKEN_VALIDATION_ERROR.toString()))
      .and()
        .body("message", equalTo("Please provide valid Facebook token."));
  }

  @Test
  public void whenValidTokenThenReturnOK() throws Exception {
    given()
      .header(AUTHORIZATION, BEARER + token)
    .when()
      .get(GET_LOCATION_URL)
    .then()
        .assertThat().statusCode(OK.getStatusCode());

  }

  @Test
  public void whenValidTokenThenReturnLocation() throws Exception {
      given()
        .header(AUTHORIZATION, BEARER + token)
      .when()
        .get(GET_LOCATION_URL)
      .then()
          .assertThat().contentType(JSON)
          .and()
          .body("size()", equalTo(1))
          .and()
          .body("[0].name", equalTo(EGNYTE_POZNAN_POLAND));
  }

  @Test
  public void whenValidTokenThenReturnLatitude() throws Exception {
    String latitude =
        given()
          .header(AUTHORIZATION, BEARER + token)
        .when()
          .get(GET_LOCATION_URL)
        .then()
          .extract().jsonPath().getString("[0].latitude");

    assertThat(Double.valueOf(latitude), closeTo(EGNYTE_POZNAN_POLAND_LATITUDE, 0.01));
  }

  @Test
  public void whenValidTokenThenReturnLongitude() throws Exception {
    String longitude =
        given()
            .header(AUTHORIZATION, BEARER + token)
        .when()
            .get(GET_LOCATION_URL)
        .then()
            .extract().jsonPath().getString("[0].longitude");

    assertThat(Double.valueOf(longitude), closeTo(EGNYTE_POZNAN_POLAND_LONGITUDE, 0.01));
  }

  @Test
  public void whenValidTokenWithMultipleLocationsThenReturnLocation() throws Exception {
    given()
      .header(AUTHORIZATION, BEARER + token)
    .when()
      .get(GET_MULTIPLE_LOCATION_URL)
    .then()
      .assertThat()
        .contentType(JSON)
        .statusCode(OK.getStatusCode())
      .and()
        .body("size()", equalTo(17))
      .and()
        .body("[0].name", equalTo("Opoczno gmina"));
  }

}